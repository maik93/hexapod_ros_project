#!/usr/bin/env python3
#
# Forward and invese kinematics. Reference systems transformations.

import rospy
from threading import Lock
from math import pi, cos, sin, atan2, asin, acos, sqrt, pow
import numpy as np
from numpy.linalg import inv, norm
# from iDynTree import vectorize, forwardKinematicsDH


# ------------- dimensions -------------
body_len = 0.15  # [m]
body_width = 0.1  # [m]
l1 = 0.052  # [m]
l2 = sqrt(pow(0.0645, 2) + pow(0.0145, 2))  # [m]
l3 = sqrt(pow(0.0832, 2) + pow(0.02, 2))  # [m]

STARTING_Z = 0.045  # initial body quote [m]


# To flatten a list of lists
def flatten(list_of_lists): return [el for sublist in list_of_lists for el in sublist]


# ------------------ Reference systems transformations ------------------


# body center reference system in terms of world system [space -> body]
def Se(pose):
    phi = pose[0]
    theta = pose[1]
    psi = pose[2]
    xe = pose[3]
    ye = pose[4]
    ze = pose[5]

    # return RPToHomogeneous(RPYToMat(phi, theta, psi), [[xe, ye, ze]])

    return np.array([[cos(theta) * cos(phi), -(cos(psi) * sin(phi)) + cos(phi) * sin(theta) * sin(psi),
                      cos(phi) * cos(psi) * sin(theta) + sin(phi) * sin(psi), xe],
                     [cos(theta) * sin(phi), cos(phi) * cos(psi) + sin(theta) * sin(phi) * sin(psi),
                      cos(psi) * sin(theta) * sin(phi) - cos(phi) * sin(psi), ye],
                     [-sin(theta), cos(theta) * sin(psi), cos(theta) * cos(psi), ze], [0, 0, 0, 1]])


# transformation from body center to each leg connection [head -> leg_j]
def Tej(j):
    if j == 1:
        return [[0, 1, 0, body_len / 2],
                [-1, 0, 0, -(body_width / 2)],
                [0, 0, 1, 0],
                [0, 0, 0, 1]]
    elif j == 2:
        return [[0, 1, 0, 0],
                [-1, 0, 0, -(body_width / 2)],
                [0, 0, 1, 0],
                [0, 0, 0, 1]]
    elif j == 3:
        return [[0, 1, 0, -(body_len / 2)],
                [-1, 0, 0, -(body_width / 2)],
                [0, 0, 1, 0],
                [0, 0, 0, 1]]
    elif j == 4:
        return [[0, -1, 0, body_len / 2],
                [1, 0, 0, body_width / 2],
                [0, 0, 1, 0],
                [0, 0, 0, 1]]
    elif j == 5:
        return [[0, -1, 0, 0],
                [1, 0, 0, body_width / 2],
                [0, 0, 1, 0],
                [0, 0, 0, 1]]
    elif j == 6:
        return [[0, -1, 0, -(body_len / 2)],
                [1, 0, 0, body_width / 2],
                [0, 0, 1, 0],
                [0, 0, 0, 1]]
    raise Exception('Tej: j must be in [1-6]')


# ------------------------- Forward kinematics -------------------------


# forward kinematic [b -> 1]
def Tb1(q1):
    # theta = vectorize([q1])
    # d = vectorize([0])
    # alpha = vectorize([pi / 2])
    # a = vectorize([l1])
    #
    # forwardKinematicsDH(theta, d, alpha, a)

    return np.array([[cos(q1), 0, sin(q1), l1 * cos(q1)],
                     [sin(q1), 0, -cos(q1), l1 * sin(q1)],
                     [0, 1, 0, 0],
                     [0, 0, 0, 1]])


# forward kinematic [b -> 1 -> 2]
def Tb2(q1, q2):
    # theta = vectorize([q1, q2])
    # d = vectorize([0, 0])
    # alpha = vectorize([pi / 2, 0])
    # a = vectorize([l1, l2])
    #
    # return forwardKinematicsDH(theta, d, alpha, a)

    return np.array([[cos(q1) * cos(q2), -cos(q1) * sin(q2), sin(q1), l1 * cos(q1) + l2 * cos(q1) * cos(q2)],
                     [cos(q2) * sin(q1), -sin(q1) * sin(q2), -cos(q1), l1 * sin(q1) + l2 * cos(q2) * sin(q1)],
                     [sin(q2), cos(q2), 0, l2 * sin(q2)],
                     [0, 0, 0, 1]])


# forward kinematic [b -> 1 -> 2 -> f]
def Tbf(q1, q2, q3):
    # theta = vectorize([q1, q2, q3])
    # d = vectorize([0, 0, 0])
    # alpha = vectorize([pi / 2, 0, 0])
    # a = vectorize([l1, l2, l3])
    #
    # return forwardKinematicsDH(theta, d, alpha, a)

    return Tb2(q1, q2).dot([[cos(q3), -sin(q3), 0, l3 * cos(q3)],
                            [sin(q3), cos(q3), 0, l3 * sin(q3)],
                            [0, 0, 1, 0],
                            [0, 0, 0, 1]])


# ------------------------- Inverse kinematics -------------------------


# pose: phi, theta, psi, xe, ye, ze
def eval_qj(des_pose, des_feet_position_j, leg_num_j):
    # q1
    S0 = Se(des_pose).dot(Tej(leg_num_j))
    Oj = inv(S0).dot(des_feet_position_j)
    q1 = atan2(Oj[1], Oj[0])

    # q3
    S1 = S0.dot(Tb1(q1))
    pS1 = S1[:, 3]
    ojs1 = (des_feet_position_j - pS1)[:3]
    q3 = -(pi - acos((l2**2 + l3**2 - norm(ojs1)**2) / (2 * l2 * l3)))

    # q2
    gamma = atan2(l3 * sin(q3), l2 + l3 * cos(q3))
    versorX1 = S1[:3, 0]
    versorY1 = S1[:3, 1]
    beta = atan2(ojs1.dot(versorY1), ojs1.dot(versorX1))
    q2 = beta - gamma

    return [q1, q2, q3]


if __name__ == "__main__":
    print("You can't run kinematic_laws.py directly! It's an internal utility library used by inverse_kinematic_server.py")
