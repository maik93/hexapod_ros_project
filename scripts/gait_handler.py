#!/usr/bin/env python3
#
# Tiptoe gait handler.

import rospy
from math import pi, sin, cos
import numpy as np
# import time

from iDynTree import vectorize, forwardKinematicsDH
import kinematic_laws

from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray, MultiArrayDimension


# Discrete time sampling interval for gait desired positions
DT = .05  # [s]


# ------------- gait values ------------
# Here is defined maximum distances for one single step: the topic '/cmd_vel' will
#   indicate a percentage of this values, not a real velocity in m/s.
# The time of a single gait is fixed, the motion velocity is imposed by gait distances.
GAIT_STEP_TIME = 0.5  # [s]
FW_MAX_DIST = 0.07  # maximum distance of one single gait (forward and lateral) [m]
LAT_MAX_DIST = 0.05  # [m]
MAX_ROT = 20. / 180. * pi  # maximum allowed rotation [rad]
Z_STEP_RAISE = kinematic_laws.STARTING_Z / 2.  # Z lift for non-resting foot [m]


# ------------ private stuff -----------
FW_MAX_VEL = FW_MAX_DIST / GAIT_STEP_TIME
LAT_MAX_VEL = LAT_MAX_DIST / GAIT_STEP_TIME
ROT_MAX_VEL = MAX_ROT / GAIT_STEP_TIME
is_moving = False
gait_state = 0.  # rest: 0 | step right: 1 | step left: -1
forward_speed_perc = 0.  # current velocities as percentual of the maximum [-1, .. , 1]
lateral_speed_perc = 0.  # [-1, .. , 1]
rotation_speed_perc = 0.  # [-1, .. , 1]

des_feet_position = np.array([[None for j in range(4)] for i in range(7)])
des_feet_msg = Float64MultiArray()
des_feet_msg.layout.dim = [MultiArrayDimension()]
des_feet_msg.layout.dim[0].stride = 1
des_feet_msg.layout.dim[0].size = 18

# -------------------- Feet rest and actual positions -------------------


# homogeneous vectors representing resting tiptoe positions,
# i.e. where the feet are when the hexapod stand still.
def feet_rest_position(leg_j):
    Tsj = kinematic_laws.Se([0, 0, 0, 0, 0, kinematic_laws.STARTING_Z]
                            ).dot(kinematic_laws.Tej(leg_j))

    d = vectorize([0, 0, 0])
    alpha = vectorize([pi / 2, 0, 0])
    a = vectorize([kinematic_laws.l1, kinematic_laws.l2, kinematic_laws.l3])

    if leg_j == 1:
        theta = vectorize([0.5236, 0.38, -2.])

    elif leg_j == 2:
        theta = vectorize([0., 0.38, -2.])

    elif leg_j == 3:
        theta = vectorize([-0.5236, 0.38, -2.])

    elif leg_j == 4:
        theta = vectorize([-0.5236, 0.38, -2.])

    elif leg_j == 5:
        theta = vectorize([0., 0.38, -2.])

    elif leg_j == 6:
        theta = vectorize([0.5236, 0.38, -2.])

    else:
        raise Exception('feet_rest_position: leg_j must be in [1-6]')

    return (Tsj.dot(forwardKinematicsDH(theta, d, alpha, a)))[:, 3]


# TODO: implement actual_feet_position
def actual_feet_position(leg_j):
    # Tsj = kinematic_laws.Se([0, 0, 0, 0, 0, kinematic_laws.STARTING_Z]).dot(kinematic_laws.Tej(leg_j))
    # return (Tsj.dot(kinematic_laws.Tbf(get_actual_qs(leg_j))))[:, 3]

    return feet_rest_position(leg_j)


# ---------------------------- Gait handling ----------------------------


# Compute next time-step position of feet leg_j
def gait_next_step(leg_j, start_position, is_left_step, tau):
    if is_left_step:
        resting_legs = [1, 3, 5]
        moving_legs = [2, 4, 6]
    else:
        resting_legs = [2, 4, 6]
        moving_legs = [1, 3, 5]

    # first the rotation, if any
    rot_angle = (rotation_speed_perc * MAX_ROT / 2) if leg_j in moving_legs else (-rotation_speed_perc * MAX_ROT / 2)
    # print("rotation angle: {} deg ({} rad)".format(rot_angle * 180 / pi, rot_angle))
    rotated_position_j = np.array([[cos(rot_angle), -sin(rot_angle), 0, 0],
                                   [sin(rot_angle), cos(rot_angle), 0, 0],
                                   [0, 0, 1, 0],
                                   [0, 0, 0, 1]]).dot(feet_rest_position(leg_j))

    # then forward and lateral motion
    if leg_j in resting_legs:  # these feet will be kept to the ground
        final_position = rotated_position_j - \
            [forward_speed_perc * FW_MAX_DIST / 2, lateral_speed_perc * LAT_MAX_DIST / 2, 0, 0]
        z_raise = 0

    elif leg_j in moving_legs:  # these feet will advance
        final_position = rotated_position_j + \
            [forward_speed_perc * FW_MAX_DIST / 2, lateral_speed_perc * LAT_MAX_DIST / 2, 0, 0]
        z_raise = Z_STEP_RAISE * sin(pi * tau)

    else:
        raise Exception('left_step: leg_j must be in [1-6]')

    # convex combination of initial and final state, plus a lift on Z.
    return start_position * (1 - tau) + final_position * tau + [0, 0, z_raise, 0]


# Callback for 'cmd_vel' topic listener
def on_move_update(msg):
    global forward_speed_perc, lateral_speed_perc, rotation_speed_perc, is_moving

    forward_speed_perc = msg.linear.x
    lateral_speed_perc = msg.linear.y
    rotation_speed_perc = msg.angular.z

    is_moving = (forward_speed_perc != 0) or (lateral_speed_perc != 0) or (rotation_speed_perc != 0)
    # rospy.loginfo('Received [%f %f %f]', msg.x, msg.y, msg.z)


# listen to cmd_vel and publish desired feet positions at rate 1/DT
if __name__ == "__main__":
    rospy.init_node('gait_handler', anonymous=False)
    rospy.Subscriber('cmd_vel', Twist, on_move_update)
    des_feet_pub = rospy.Publisher('des_feet_position', Float64MultiArray, queue_size=1)

    start_position = np.zeros(7 * 4).reshape(7, 4)
    tau = 0.  # normalized time interval [0, .. , 1]

    # initial desired feet positions to allow inverse_kinematic_server to run
    des_feet_position[1:, :] = np.array([feet_rest_position(leg_j) for leg_j in range(1, 7)])

    print("gait_handler is online at rate {}Hz.".format(1 / DT))

    actual_t = rospy.Time.now()
    rate = rospy.Rate(1 / DT)  # Hz
    try:
        while not rospy.is_shutdown():
            # start_time = time.time()

            actual_t, prev_t = rospy.Time.now(), actual_t
            real_dt = (actual_t - prev_t).secs + (actual_t - prev_t).nsecs * 1.0e-9  # [s]

            # print("is_moving: %d ~ gait_state: %d ~ tau=%f" % (is_moving,gait_state,tau))
            if not is_moving and tau == 0.:  # rest
                des_feet_position[1:, :] = np.array([feet_rest_position(leg_j) for leg_j in range(1, 7)])
                gait_state = 0

            else:
                if gait_state == 0:  # first move, update start_position
                    for leg_j in range(1, 7):
                        start_position[leg_j] = actual_feet_position(leg_j)
                    gait_state = 1  # then start with a right step
                    tau = 0.

                tau += real_dt / GAIT_STEP_TIME
                if tau >= 1.:
                    # switch from left to right step and vice-versa
                    for leg_j in range(1, 7):  # store last positions as new starts
                        start_position[leg_j] = des_feet_position[leg_j]
                    gait_state *= -1  # invert resting foot
                    tau = 0.

                # compute feet positions of next time-step
                des_feet_position[1:, :] = \
                    np.array([gait_next_step(leg_j, start_position[leg_j], gait_state == 1, tau) for leg_j in range(1, 7)])

            des_feet_msg.data = kinematic_laws.flatten(des_feet_position[1:, :])  # take ony not None values
            des_feet_pub.publish(des_feet_msg)

            # print("[Evaluation time: %s ms]\n" % (time.time() - start_time)*1000)
            # it seems to run at ~2ns per loop
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
