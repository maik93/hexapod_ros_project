#!/usr/bin/env python3
#
# Core program of the hexapod motion. Works together with 'gait_handler' node.
#   Listening 'cmd_vel' (in 'gait_handler') and 'des_pose' topics, inverse kinematic
#   is solved usign 'kinematic_laws.eval_qj' function, given tiptoe positions that gait_handler
#   computes in order to move in the desired direction with the desired body pose.
#
#   Resulting joint desired angles are published in 'hex_legs_controller/command'
#   to interface with Gazebo simulator, or the real robot (though 'dynamixel_driver' node).

import rospy
# import time
import numpy as np

from std_msgs.msg import Float64MultiArray, MultiArrayDimension

import kinematic_laws
from hexapod.msg import Pose


# feet's actual positions (will be populated by gait_handler)
# I know, there's one extra, but I'm going crazy with indexes and leg_j
des_feet_position = np.array([[None for j in range(4)] for i in range(7)])


# Update frequency for joints desired angles (pay attention to
# gait_handler.DT value, something faster than that is just useless)
DT = .05  # [s]

servo_positions = Float64MultiArray()
servo_positions.layout.dim = [MultiArrayDimension()]
servo_positions.layout.dim[0].stride = 1
servo_positions.layout.dim[0].size = 18


# Evaluate joint angles for given body and tiptoes position and publish to the motors
def eval_inversion(motor_pub):
    if not des_feet_position.any():
        return
    try:
        list_of_desired_angles = \
            [kinematic_laws.eval_qj(des_pose, des_feet_position[j], j) for j in range(1, 7)]
        servo_positions.data = kinematic_laws.flatten(list_of_desired_angles)
        motor_pub.publish(servo_positions)
        # rospy.loginfo(servo_positions)  # print(in log file and rosout)
    except ValueError:
        print(" !! POSE NOT FEASABLE !!")
        pass


# Callback for 'des_pose' topic listener
def update_des_pose(des_pose_msg):
    global des_pose

    des_pose = [des_pose_msg.phi, des_pose_msg.theta, des_pose_msg.psi,
                des_pose_msg.x, des_pose_msg.y, des_pose_msg.z]


def update_des_feet_position(des_feet_msg):
    global des_feet_position

    des_feet_position[1:, :] = np.reshape(des_feet_msg.data, (6, 4))


# listen to des_pose and publish desired joint angles at rate 1/DT
if __name__ == "__main__":
    des_pose = [0, 0, 0, 0, 0, kinematic_laws.STARTING_Z]

    rospy.init_node('inverse_kinematic_server', anonymous=False)
    rospy.Subscriber('des_pose', Pose, update_des_pose)
    rospy.Subscriber('des_feet_position', Float64MultiArray, update_des_feet_position)
    motor_controller_pub = rospy.Publisher('/hex_legs_controller/command', Float64MultiArray, queue_size=1)

    print("inverse_kinematic_server is online.")

    rate = rospy.Rate(1 / DT)  # Hz
    try:
        while not rospy.is_shutdown():  # check for Ctrl-C
            # start_time = time.time()

            eval_inversion(motor_controller_pub)

            # print("[Evaluation time: %s sec]" % (time.time() - start_time))
            # it seems to run at ~1ms
            rate.sleep()  # "rate" (instead of "time") can handle simulated time

    except rospy.ROSInterruptException:
        pass
