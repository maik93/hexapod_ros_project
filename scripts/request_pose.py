#!/usr/bin/env python3
#
# One-time request for position and orientation of body center in local reference frame.
#
# Usage example:
# rosrun hexapod request_pose.py 0 0 0.045 0 0 0


import sys
import rospy
from numpy import empty

from hexapod.msg import Pose


def usage():
    return "wrong usage, you should say 'inverse_kinematic.py <int_x> <int_y> <int_z> <int_psi> <int_theta> <int_fi>'"


if __name__ == "__main__":
    rospy.init_node('request_pose', anonymous=False)
    pub = rospy.Publisher('des_pose', Pose, queue_size=1)

    # check arguments passed
    if len(sys.argv) == 7 or len(sys.argv) == 9:
        des_pose = Pose()
        des_pose.x = float(sys.argv[1])
        des_pose.y = float(sys.argv[2])
        des_pose.z = float(sys.argv[3])
        des_pose.phi = float(sys.argv[4])
        des_pose.theta = float(sys.argv[5])
        des_pose.psi = float(sys.argv[6])

        print("Requesting pose:\n-> (x,y,z) = [%s, %s, %s]\n-> (Y,P,R) = [%s, %s, %s]" % (des_pose.x, des_pose.y, des_pose.z, des_pose.phi, des_pose.theta, des_pose.psi))
        rate = rospy.Rate(10)  # Hz
        subscribers = 0
        try:
            while not rospy.is_shutdown() and subscribers == 0:  # check for Ctrl-C
                subscribers = pub.get_num_connections()
                if subscribers > 0:
                    pub.publish(des_pose)
                rate.sleep()
        except rospy.ROSInterruptException:
            pass
    else:
        print(usage())
        # print("you've passed %d arguments" % len(sys.argv))
        sys.exit(1)
