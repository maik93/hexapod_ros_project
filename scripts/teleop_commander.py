#!/usr/bin/env python3
#
# ArbotiX Commander Joystick control:
# https://learn.trossenrobotics.com/index.php/getting-started-with-the-arbotix/12-arbotix-commander-setup
# https://github.com/Interbotix/arbotix/blob/master/ArbotiX%20Sketches/Commander/Commander.ino

# The message structure is this (8 bytes):
# 0: 0xff
# 1: joy_right_vertical
# 2: joy_right_horizontal
# 3: joy_left_vertical
# 4: joy_left_horizontal
# 5: buttons
# 6: 0
# 7: checksum: 255 - (right_V + right_H + left_V + left_H + buttons) % 256

import sys
import serial
import time
import rospy
import numpy as np
from math import pi
from struct import unpack

import kinematic_laws

from hexapod.msg import Pose
from geometry_msgs.msg import Vector3, Twist

ROBOT_NS = "aragog"

list_of_commands = """
WALKING: left analog
BODY ROTATION: right analog

BUTTONS (from left to right):
  1: body backward
  2: body forward
  
  3: Z decrease
  4: Z increase
  
  5: body left
  6: body right
  
  Top Left: rotate left
  Top Right: rotate right

CTRL-C or ESC to quit
"""

# --- pose parameters ---
max_x = .2
max_y = .05
max_z = .3
min_z = 0.
max_phi = pi / 32.
max_theta = pi / 32.
max_psi = pi / 32.

inc_x = max_x / 160.
inc_y = max_y / 80.
inc_z = .0005
inc_phi = pi / 256.
# -----------------------

# buttons bit position
BUT_LT = 0b10000000
BUT_RT = 0b01000000
BUT_R1 = 0b00100000
BUT_R2 = 0b00010000
BUT_R3 = 0b00001000
BUT_L4 = 0b00000100
BUT_L5 = 0b00000010
BUT_L6 = 0b00000001
buttons = [BUT_R1, BUT_R2, BUT_R3, BUT_L4, BUT_L5, BUT_L6, BUT_RT, BUT_LT]


# return true if the desired button is present in the passed message
def isPressed(received_msg, button_id):
    return received_msg[4] & button_id == button_id


# enshure that no values has module bigger than max allowed
def enforceLimits(val, max_val, min_val=None):
    if min_val is None:
        min_val = -max_val

    if(val > max_val):
        return max_val
    if(val < min_val):
        return min_val
    return val


def joy_callback(joy_msg):
    global current_x
    global current_y
    global current_z
    global current_phi

    # analogs go from 26 to 230, with differents resting values
    left_analog = np.subtract(joy_msg[2:4], [126, 129])  # 0 in rest positions
    [direction.linear.x, direction.linear.y] = left_analog / 102.0  # 1 at maximum inclinations
    direction.linear.y = -direction.linear.y

    right_analog = np.subtract(joy_msg[:2], [128, 127])
    [theta, psi] = np.dot([[max_psi, 0], [0, max_theta]], right_analog) / 102.0

    # buttons
    if isPressed(joy_msg, BUT_R2):  # body forward
        current_x = enforceLimits(current_x + inc_x, max_x)
    elif isPressed(joy_msg, BUT_R1):  # body backward
        current_x = enforceLimits(current_x - inc_x, max_x)
    if isPressed(joy_msg, BUT_L5):  # body left
        current_y = enforceLimits(current_y + inc_y, max_y)
    elif isPressed(joy_msg, BUT_L6):  # body right
        current_y = enforceLimits(current_y - inc_y, max_y)
    if isPressed(joy_msg, BUT_R3):  # body down
        current_z = enforceLimits(current_z - inc_z, max_z, min_z)
    elif isPressed(joy_msg, BUT_L4):  # body up
        current_z = enforceLimits(current_z + inc_z, max_z, min_z)

    # LT and RT (body rotation)
    # elif isPressed(joy_msg, BUT_LT):  # rotate left
    #     current_phi = enforceLimits(current_phi + inc_phi, max_phi)
    # if isPressed(joy_msg, BUT_RT):  # rotate right
    #     current_phi = enforceLimits(current_phi - inc_phi, max_phi)

    # LT and RT (pose rotation)
    elif isPressed(joy_msg, BUT_LT):  # rotate left
        direction.angular.z = 1
    if isPressed(joy_msg, BUT_RT):  # rotate right
        direction.angular.z = -1
    current_phi = 0
    if isPressed(joy_msg, BUT_LT) or isPressed(joy_msg, BUT_RT):
        direction.linear.x = 0
        direction.linear.y = 0
    else:
        direction.angular.z = 0

    pose = Pose()
    pose.x = current_x
    pose.y = current_y
    pose.z = current_z
    pose.phi = current_phi
    pose.theta = theta
    pose.psi = psi

    try:
        dir_pub.publish(direction)
        pose_pub.publish(pose)

    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)


if __name__ == "__main__":
    global current_x
    global current_y
    global current_z
    global current_phi
    current_x = 0.
    current_y = 0.
    current_z = kinematic_laws.STARTING_Z
    current_phi = 0.

    rospy.init_node('teleop_pose', anonymous=False)
    pose_pub = rospy.Publisher(f'/{ROBOT_NS}/des_pose', Pose, queue_size=1)
    dir_pub = rospy.Publisher(f'/{ROBOT_NS}/cmd_vel', Twist, queue_size=1)

    # connect to Commander through XBee
    ser = serial.Serial('/dev/ttyS0', 38400, timeout=.5)

    # initial pose
    pose = Pose()
    pose.x = current_x
    pose.y = current_y
    pose.z = current_z
    pose.phi = 0
    pose.theta = 0
    pose.psi = current_phi
    pose_pub.publish(pose)

    # gait direction
    direction = Twist()
    direction.linear = Vector3()
    direction.linear.x = 0
    direction.linear.y = 0
    direction.angular = Vector3()
    direction.angular.z = 0

    print(list_of_commands)

    # Commander communicate at 30Hz, so this loop is enforced to that frequency by ser.read(..)
    try:
        while not rospy.is_shutdown():  # check for Ctrl-C
            header = ser.read()
            if len(header) == 0 or ord(header) != 0xff:  # check header
                continue  # close this iteration

            incoming = ser.read(7)  # Commander sends packs of 8 bytes (including header)
            msg = unpack('7B', incoming)  # intepret as usigned chars
            # print(msg[:5]  # excluding extra and checksum)

            # check checksum
            if 255 - (msg[0] + msg[1] + msg[2] + msg[3] + msg[4]) % 256 == msg[6]:
                joy_callback(msg)

    except (rospy.ROSInterruptException, serial.serialutil.SerialException):
        pass
