#!/usr/bin/env python3

import os
import rospy

from gazebo_msgs.msg import ContactsState


def is_touching(leg_j):
    global bumper_pressed
    if len(bumper_pressed) == 6:
        return bumper_pressed[leg_j - 1]
    else:
        raise Exception(
            "variable 'bumper_pressed' seems to be empty, are you running bumper_handler thread?")


def check_contact(contact_msg, leg_j):
    global bumper_pressed

    if len(contact_msg.states) > 0:
        bumper_pressed[leg_j - 1] = True
    else:
        bumper_pressed[leg_j - 1] = False


def tip1_bumper_callback(contact_msg):
    check_contact(contact_msg, 1)


def tip2_bumper_callback(contact_msg):
    check_contact(contact_msg, 2)


def tip3_bumper_callback(contact_msg):
    check_contact(contact_msg, 3)


def tip4_bumper_callback(contact_msg):
    check_contact(contact_msg, 4)


def tip5_bumper_callback(contact_msg):
    check_contact(contact_msg, 5)


def tip6_bumper_callback(contact_msg):
    check_contact(contact_msg, 6)


if __name__ == "__main__":
    global bumper_pressed
    bumper_pressed = [False, False, False, False, False, False]

    rospy.init_node('bumper_checker', anonymous=False)
    rospy.Subscriber('tip1_bumper_vals', ContactsState, tip1_bumper_callback)
    rospy.Subscriber('tip2_bumper_vals', ContactsState, tip2_bumper_callback)
    rospy.Subscriber('tip3_bumper_vals', ContactsState, tip3_bumper_callback)
    rospy.Subscriber('tip4_bumper_vals', ContactsState, tip4_bumper_callback)
    rospy.Subscriber('tip5_bumper_vals', ContactsState, tip5_bumper_callback)
    rospy.Subscriber('tip6_bumper_vals', ContactsState, tip6_bumper_callback)

    # rate = rospy.Rate(5)  # Hz
    # while not rospy.is_shutdown():
    #     os.system('clear')
    #     print(bumper_pressed)
    #     rate.sleep()

    # contacts_pub = rospy.Publisher('bumper_pressed', TipContacts, queue_size=1)
    # try:
    #     contacts_pub.publish(bumper_pressed)
    # except rospy.ServiceException as e:
    #     print("Service call failed: %s" % e)

    rospy.spin()
