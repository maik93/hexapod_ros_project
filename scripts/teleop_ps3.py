#!/usr/bin/env python3
#
# PS3 Joystick control.

# http://wiki.ros.org/ps3joy
#
# Joy.buttons[4]    -> up
# Joy.buttons[5]    -> right
# Joy.buttons[6]    -> down
# Joy.buttons[7]    -> left
# Joy.axes[:2]      -> left_analog
# Joy.axes[2:4]     -> right_analog
# Joy.axes[16:19]   -> RPY from accelerometer (the last one seems uncontrollable)
# Joy.buttons[10]   -> L1
# Joy.buttons[8]    -> L2
# Joy.buttons[11]   -> R1
# Joy.buttons[9]    -> R2


import sys
import rospy
import numpy
from math import pi

import kinematic_laws

from hexapod.msg import Pose
from geometry_msgs.msg import Vector3, Twist
from sensor_msgs.msg import Joy

ROBOT_NS = "aragog"

# TODO: explain joy commands
# msg = """
# Reading from the keyboard  and Publishing to Pose!
# ------------- MAIN BODY -------------
# Rotating around:
#   q  w  e   ->   -yaw  +pitch  +yaw
#   a  s  d   ->  -roll  -pitch  +roll
#
# Moving:
#      ^      ->      +x
#   <  v  >   ->  +y  -x  -y
# Keypad 1: +z
# Keypad 0: -z
#
# -------------- WALKING --------------
# From keypad:
#      8      ->      forw
#   4  5  6   -> left stop right
#      2      ->      back
#
# CTRL-C or ESC to quit
# """

# --- pose parameters ---
max_x = .2
max_y = .05
max_z = .3
min_z = 0.
max_phi = pi / 8.
max_theta = pi / 8.
max_psi = pi / 8.

inc_x = max_x / 20.
inc_y = max_y / 20.
inc_z = .0025
inc_phi = pi / 128.
# -----------------------


# enshure that no values has module bigger than max allowed
def enforceLimits(val, max_val, min_val=None):
    if min_val is None:
        min_val = -max_val

    if(val > max_val):
        return max_val
    if(val < min_val):
        return min_val
    return val


def joy_callback(joy_msg):
    global current_x
    global current_y
    global current_z
    global current_phi

    # print(joy_msg.axes)
    # print(joy_msg.buttons)

    # left_analog
    [direction.linear.y, direction.linear.x] = joy_msg.axes[:2]

    # right_analog
    [psi, theta] = numpy.dot([[max_psi, 0], [0, max_theta]], joy_msg.axes[2:4])

    # arrows
    if joy_msg.buttons[4]:  # up
        current_x = enforceLimits(current_x + inc_x, max_x)
    elif joy_msg.buttons[6]:  # down
        current_x = enforceLimits(current_x - inc_x, max_x)
    if joy_msg.buttons[7]:  # left
        current_y = enforceLimits(current_y + inc_y, max_y)
    elif joy_msg.buttons[5]:  # right
        current_y = enforceLimits(current_y - inc_y, max_y)

    # L1 and L2
    if joy_msg.buttons[10]:
        current_z = enforceLimits(current_z + inc_z, max_z, min_z)
    elif joy_msg.buttons[8]:
        current_z = enforceLimits(current_z - inc_z, max_z, min_z)

    # R1 and R2
    if joy_msg.buttons[11]:
        current_phi = enforceLimits(current_phi + inc_phi, max_phi)
    elif joy_msg.buttons[9]:
        current_phi = enforceLimits(current_phi - inc_phi, max_phi)

    pose = Pose()
    pose.x = current_x
    pose.y = current_y
    pose.z = current_z
    pose.phi = current_phi
    pose.theta = theta
    pose.psi = psi

    try:
        dir_pub.publish(direction)
        pose_pub.publish(pose)

    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)


if __name__ == "__main__":
    global current_x
    global current_y
    global current_z
    global current_phi
    current_x = 0.
    current_y = 0.
    current_z = kinematic_laws.STARTING_Z
    current_phi = 0.

    rospy.init_node('teleop_pose', anonymous=False)
    pose_pub = rospy.Publisher(f'/{ROBOT_NS}/des_pose', Pose, queue_size=1)
    dir_pub = rospy.Publisher(f'/{ROBOT_NS}/cmd_vel', Twist, queue_size=1)
    rospy.Subscriber('joy', Joy, joy_callback)

    # initial pose
    pose = Pose()
    pose.x = current_x
    pose.y = current_y
    pose.z = current_z
    pose.phi = 0
    pose.theta = 0
    pose.psi = current_phi
    pose_pub.publish(pose)

    # gait direction
    direction = Twist()
    direction.linear = Vector3()
    direction.linear.x = 0
    direction.linear.y = 0
    direction.angular = Vector3()
    direction.angular.z = 0

    rospy.spin()
