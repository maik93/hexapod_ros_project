#!/usr/bin/env python3
#
# Keyboard control.

import time
# import thread
import curses
import rospy
from math import pi

import kinematic_laws

from hexapod.msg import Pose
from geometry_msgs.msg import Vector3, Twist

ROBOT_NS = "aragog"

msg = """
  ------------- MAIN BODY -------------
  Rotate around:
    q  w  e   ->   -yaw  +pitch  +yaw
    a  s  d   ->  -roll  -pitch  +roll

  Move:                                VELOCITIES %
       ^      ->      +x              forward:  {:1.2f}
    <  v  >   ->  +y  -x  -y          lateral:  {:1.2f}
  Keypad 1: +z                        rotation: {:1.2f}
  Keypad 0: -z
  -------------- WALKING --------------
  From keypad:
    7  8  9   -> rot_L  forw  rot_R
    4  5  6   ->  left  stop  right
       2      ->        back

         CTRL-C or ESC to quit
"""

# main body pose increment at each key pressed
inc_angle = pi / 128.
inc_pos = .0025

# walking increment at each key pressed
inc_vel_fw = 0.05
inc_vel_lat = 0.05
inc_vel_rot = 0.05

# maximum absolute rotation velocity allowed while walking (otherwhise stop and rotate only)
walking_rot_threshold = 0.35  # where the upper limit is 1, so it's like a percentage

# key links, only for main body's pose movements
poseMoves = {
    # <key>: (dx, dy, dz, dphi, dtheta, dpsi)
    97: (0., 0., 0., 0., 0., -inc_angle),  # a
    100: (0., 0., 0., 0., 0., inc_angle),  # d
    115: (0., 0., 0., 0., -inc_angle, 0.),  # s
    119: (0., 0., 0., 0., inc_angle, 0.),  # w
    113: (0., 0., 0., inc_angle, 0., 0.),  # q
    101: (0., 0., 0., -inc_angle, 0., 0.),  # e
    259: (2 * inc_pos, 0., 0., 0., 0., 0.),  # up
    258: (-2 * inc_pos, 0., 0., 0., 0., 0.),  # down
    260: (0., -inc_pos, 0., 0., 0., 0.),  # left
    261: (0., inc_pos, 0., 0., 0., 0.),  # right
    49: (0., 0., 6 * inc_pos, 0., 0., 0.),  # keypad_1
    48: (0., 0., -6 * inc_pos, 0., 0., 0.),  # keypad_0
}

# maximum walking limits
max_vel_fw = 1
max_vel_lat = 1
max_vel_rot = 1

# maximum limits for main body pose
max_x = .2
max_y = .05
max_z = kinematic_laws.STARTING_Z * 2
min_z = 0.05
max_phi = pi / 8.
max_theta = pi / 8.
max_psi = pi / 8.


# enshure that no values has module bigger than max allowed
def enforceLimits(val, max_val, min_val=None):
    if min_val is None:
        min_val = -max_val

    if(val > max_val):
        return max_val
    if(val < min_val):
        return min_val
    return val


def main(screen):
    rospy.init_node('teleop_pose', anonymous=False)
    pose_pub = rospy.Publisher(f'/{ROBOT_NS}/des_pose', Pose, queue_size=1)
    dir_pub = rospy.Publisher(f'/{ROBOT_NS}/cmd_vel', Twist, queue_size=1)

    direction = Twist()
    direction.linear = Vector3()
    direction.linear.x = 0  # forward
    direction.linear.y = 0  # lateral
    direction.angular = Vector3()
    direction.angular.z = 0  # rotation

    pose = Pose()
    pose.x = 0
    pose.y = 0
    pose.z = kinematic_laws.STARTING_Z
    pose.phi = 0
    pose.theta = 0
    pose.psi = 0

    try:
        # graphic stuff
        curses.noecho()
        curses.curs_set(0)
        screen.keypad(1)
        screen.scrollok(1)
        screen.addstr(msg.format(0, 0, 0))
        screen.nodelay(1)

        rate = rospy.Rate(50)  # Hz
        while not rospy.is_shutdown():
            # get last key pressed (after cleaning-up the buffer)
            curses.flushinp()
            time.sleep(0.01)
            key = screen.getch()
            # if (key != -1): print(key)

            if (key == 27):  # Esc
                break

            # walking commands
            elif (key == 53):  # numpad center (5)
                direction.linear.x = 0
                direction.linear.y = 0
                direction.angular.z = 0
            elif (key == 56):  # numpad up (8)
                direction.linear.x = enforceLimits(direction.linear.x + inc_vel_fw, max_vel_fw)
                direction.angular.z = 0
            elif (key == 50):  # numpad down (2)
                direction.linear.x = enforceLimits(direction.linear.x - inc_vel_fw, max_vel_fw)
                direction.angular.z = 0
            elif (key == 52):  # numpad left (4)
                direction.linear.y = enforceLimits(direction.linear.y + inc_vel_lat, max_vel_lat)
                direction.angular.z = 0
            elif (key == 54):  # numpad right (6)
                direction.linear.y = enforceLimits(direction.linear.y - inc_vel_lat, max_vel_lat)
                direction.angular.z = 0
            elif (key == 55):  # numpad rotate left (7)
                direction.angular.z = enforceLimits(direction.angular.z + inc_vel_rot, max_vel_rot)
            elif (key == 57):  # numpad rotate right (9)
                direction.angular.z = enforceLimits(direction.angular.z - inc_vel_rot, max_vel_rot)

            # if rotating too much it can't walk at the same time
            if (abs(direction.angular.z) > walking_rot_threshold):
                direction.linear.x = 0
                direction.linear.y = 0

            dir_pub.publish(direction)

            # pose commands
            if key in poseMoves.keys():
                pose.x = enforceLimits(pose.x + poseMoves[key][0], max_x)
                pose.y = enforceLimits(pose.y + poseMoves[key][1], max_y)
                pose.z = enforceLimits(pose.z + poseMoves[key][2], max_z, min_z)
                pose.phi = enforceLimits(pose.phi + poseMoves[key][3], max_phi)
                pose.theta = enforceLimits(pose.theta + poseMoves[key][4], max_theta)
                pose.psi = enforceLimits(pose.psi + poseMoves[key][5], max_psi)

            pose_pub.publish(pose)

            screen.addstr(0, 0, msg.format(direction.linear.x, direction.linear.y, direction.angular.z))
            screen.refresh()

            rate.sleep()  # "rate" (instead of "time") can handle simulated time

    except (rospy.ServiceException, rospy.ROSInterruptException) as e:
        # print("Service call failed: %s" % e)
        pass

    # finally:  # executes regardless exceptions
        # curses.endwin()


if __name__ == "__main__":
    curses.wrapper(main)
