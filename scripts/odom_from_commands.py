#!/usr/bin/env python3
#
# Passive odometry for the exapod: computed only listening desired body pose and velocities.
# Composed in two steps: the message "des_pose" contains the pose of the main body respect to
#   a local reference frame "rel_origin", thus a first transformation rel_origin -> base_link
#   is evaluated; the message "cmd_vel" contains the desired absolute velocity of
#   that main body, thus a dead reckoning is computed to publish the second transformation
#   odom_from_comm -> rel_origin.
# While the first transform is supposed to be with zero steady-state error (because directly
#   generated from desired poses), the latter is prone to drifts, caused by the evaluation of
#   integrals (estimation of positions from desired velocities).
# Note that here we're using desired positions and velocities, not the actual ones, and this
#   introduces a further of inaccuracy.
# THIS APPROACH HAS A LARGE LIMITATION: there's no way to evaluate variations of altitude!

import tf
import rospy
from math import cos, sin
import numpy as np

import kinematic_laws
import gait_handler

from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseWithCovarianceStamped as Pose_with_cov  # original pose message with Covariance matrix and timestap, for the outgoing odometry publisher
from hexapod.msg import Pose as Pose_hexapod  # hexapod package custom message, for the incoming commands

ROBOT_NS = "aragog"

# store relative poses of the main body
def pose_update(msg):
    global rel_x, rel_y, rel_z, rel_phi, rel_theta, rel_psi

    rel_x = msg.x
    rel_y = msg.y
    rel_z = msg.z
    rel_phi = msg.phi
    rel_theta = msg.theta
    rel_psi = msg.psi


def vel_update(msg):
    global forward_speed, lateral_speed, rotation_speed

    # empirically corrected using Gazebo and Rviz
    forward_speed = msg.linear.x * gait_handler.LAT_MAX_VEL  # theoretically it should be FW_MAX_VEL
    lateral_speed = msg.linear.y * gait_handler.LAT_MAX_VEL * .85
    rotation_speed = msg.angular.z * gait_handler.ROT_MAX_VEL * 2 / 3 / (3.75 / 4)


if __name__ == '__main__':
    global rel_x, rel_y, rel_z, rel_phi, rel_theta, rel_psi
    global forward_speed, lateral_speed, rotation_speed

    rospy.init_node('odom_from_commands')
    rospy.Subscriber(f'/{ROBOT_NS}/des_pose', Pose_hexapod, pose_update)
    rospy.Subscriber(f'/{ROBOT_NS}/cmd_vel', Twist, vel_update)
    odom_pub = rospy.Publisher('odometry/from_commands', Pose_with_cov, queue_size=1)
    br = tf.TransformBroadcaster()

    print("odom_from_commands is online.")

    # main body relative position respect to rel_origin
    rel_x, rel_y, rel_z, rel_phi, rel_theta, rel_psi = 0, 0, kinematic_laws.STARTING_Z, 0, 0, 0

    # absolute velocity and position respect to world
    forward_speed, lateral_speed, rotation_speed = 0, 0, 0
    world_x, world_y, world_rot = 0, 0, 0

    actual_t = rospy.Time.now()

    # odometry message topic
    odom = Pose_with_cov()
    odom.header.seq = 0
    odom.header.frame_id = "odom"

    # fixed covariance matrix: 5mm for positions, 1deg for rotations
    v = np.append(0.005 * np.ones(3), np.pi / 180. * np.ones(3))
    odom.pose.covariance = np.diag(v).reshape(36)

    rate = rospy.Rate(50)  # [Hz]
    try:
        while not rospy.is_shutdown():  # check for Ctrl-C
            actual_t, prev_t = rospy.Time.now(), actual_t

            dt = (actual_t - prev_t).secs + (actual_t - prev_t).nsecs * 1.0e-9  # [s]

            world_rot += rotation_speed * dt
            c_w, s_w = cos(world_rot), sin(world_rot)
            world_x += c_w * forward_speed * dt - s_w * lateral_speed * dt
            world_y += s_w * forward_speed * dt + c_w * lateral_speed * dt

            # publish TF from rel_origin to base_link
            # br.sendTransform((rel_x, rel_y, rel_z),
            #                  tf.transformations.quaternion_from_euler(rel_psi, rel_theta, rel_phi),
            #                  actual_t, "base_link", "rel_origin")

            # # then publish TF from odom_from_comm to rel_origin
            # br.sendTransform((world_x, world_y, 0),
            #                  tf.transformations.quaternion_from_euler(0, 0, world_rot),
            #                  actual_t, "rel_origin", "odom_from_comm")

            # timestamp and reference system
            odom.header.seq += 1
            odom.header.stamp = actual_t

            # rel_origin -> odom_from_comm homogeneous transformation
            p_odomToRel = (world_x, world_y, 0)
            T_odomToRel = tf.transformations.euler_matrix(0, 0, world_rot)
            T_odomToRel[:3, 3] = p_odomToRel

            # base_link -> rel_origin homogeneous transformation
            p_relToBase = (rel_x, rel_y, rel_z)
            T_relToBase = tf.transformations.euler_matrix(rel_psi, rel_theta, rel_phi)
            T_relToBase[:3, 3] = p_relToBase

            # pose of body in odom frame: composition of base_link -> rel_origin and rel_origin -> odom_from_comm
            T_odomToBase = np.dot(T_odomToRel, T_relToBase)

            # final position and rotation of odom -> base_link
            (odom.pose.pose.position.x, odom.pose.pose.position.y, odom.pose.pose.position.z) = T_odomToBase[:3, 3]
            (odom.pose.pose.orientation.x, odom.pose.pose.orientation.y, odom.pose.pose.orientation.z, odom.pose.pose.orientation.w) = \
                tf.transformations.quaternion_from_matrix(T_odomToBase)

            odom_pub.publish(odom)

            rate.sleep()
    except rospy.ROSInterruptException:
        pass
