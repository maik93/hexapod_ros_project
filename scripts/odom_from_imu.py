#!/usr/bin/env python3
#
# Active odometry for the exapod: computed through IMU data.
# Composed as normal: orientation is stored directly, while absolute position is evaluated
#   by double integrating linear accelerations (inertial navigation).
# Here is one single transformation: odom_imu -> base_link

import tf
import rospy
from math import cos, sin

import kinematic_laws
import gait_handler

from sensor_msgs.msg import Imu


# store incoming IMU data (50Hz in Gazebo)
def imu_update(msg):
    global rot_quaternion, body_acc_omog

    # IMU message publishes orientation as Quaternion, but we can work with it
    rot_quaternion = (msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w)

    # homogeneous acceleration vector in body reference system
    body_acc_omog = (msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z, 0)


if __name__ == '__main__':
    global rot_quaternion, body_acc_omog

    rospy.init_node('odom_from_imu')
    rospy.Subscriber('imu_data', Imu, imu_update)
    br = tf.TransformBroadcaster()

    print("odom_from_imu is online.")

    # main body orientation respect to world
    rot_quaternion = (1, 1, 1, 0)

    # absolute acceleration and position respect to world (homogeneous vectors)
    body_acc_omog = (0, 0, 0, 0)
    world_position = (0, 0, 0, 1)

    actual_t = rospy.Time.now()

    rate = rospy.Rate(30)  # [Hz]
    try:
        while not rospy.is_shutdown():  # check for Ctrl-C
            actual_t, prev_t = rospy.Time.now(), actual_t

            dt = (actual_t - prev_t).secs + (actual_t - prev_t).nsecs * 1.0e-9  # [s]

            rot_mat_omog = tf.transformations.quaternion_matrix(rot_quaternion)  # homogeneous rotation matrix
            # evaluate world_position here ...

            # publish transform from odom_imu to base_link
            br.sendTransform((0, 0, 0), rot_quaternion, actual_t, "base_link", "odom_imu")

            rate.sleep()
    except rospy.ROSInterruptException:
        pass
