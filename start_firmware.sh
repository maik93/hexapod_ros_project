#!/usr/bin/env bash

ros_version='noetic'
current_dir=`pwd -P`
script_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo bash -c "source /opt/ros/$ros_version/setup.bash && source $script_dir/../../devel/setup.bash && roslaunch hexapod real_robot_base.launch"
