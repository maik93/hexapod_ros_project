# Hexapod ROS project

## Installation (on Ubuntu)
Tested on:
- Ubuntu 16.04, R.O.S. Kinetic and Lunar
- Ubuntu 18.04.1. R.O.S. Melodic (**no Realsense RS200 support**)
- Ubuntu 20.04.1. R.O.S. Noetic (**no Realsense RS200 support**)

### Initial setup
- first follow this guide: http://wiki.ros.org/noetic/Installation/Ubuntu
- then create a workspace typing:

```sh
mkdir -p ~/aragog_ws/src
cd ~/aragog_ws/
catkin_make
```
- and add this row at the end of `~/.bashrc`:

```
source ~/aragog_ws/devel/setup.bash
```

### Clone this repo and build
You've to clone this repo, its submodules and other needed libraries in `~/aragog_ws/src` (or wherever you've your ROS workspace). To do this run:

#### for R.O.S. Kinetic/Lunar (Ubuntu 16.04 LTS):
Main system (firmware and manual controls):
```sh
cd ~/aragog_ws/src
git clone --recurse-submodules https://gitlab.com/maik93/hexapod_ros_project.git
git clone https://github.com/ros-controls/ros_controllers.git
cd ros_controllers && git checkout kinetic-devel && cd ..
git clone https://github.com/Maik93/realsense_gazebo_plugin.git
cd realsense_gazebo_plugin && git checkout kinetic && cd ..
sudo apt-get install ros-kinetic-rtabmap-ros ros-kinetic-robot-localization
```
Vision system (with Realsense RS200):
```sh
cd ~/aragog_ws/src
git clone https://github.com/intel-ros/realsense.git
cd realsense && git checkout 1.8.0 && cd ..
```

#### for R.O.S. Melodic or Noetic (Ubuntu 18.04 or 20.04 LTS - **without Realsense**):
Only main system (firmware and manual controls):
```sh
cd ~/aragog_ws/src
git clone --recurse-submodules https://gitlab.com/maik93/hexapod_ros_project.git
git clone https://github.com/ros-controls/ros_controllers.git
git clone https://github.com/Maik93/realsense_gazebo_plugin.git
cd realsense_gazebo_plugin && git checkout melodic && cd ..
sudo apt-get install ros-melodic-rtabmap-ros ros-melodic-robot-localization
```

#### Install dependecies and build
```sh
cd ~/aragog_ws/
rosdep install --from-paths src --ignore-src -r -y
catkin_make
```

Only on the real robot, the GPIO libary is needed, so run this:
```sh
cd ~/aragog_ws/src/hexapod_ros_project/sources/jetsonDynamixel
./install.sh
```

### Play with it
Main behavior are managed by `launch/*.launch` files.

Use `ctrl+C` in terminal to close everything at once (usually it take some seconds to abort). I think it's a good practice to not save rViz configurations on exit (if it asks), but as you wish.

## Simulator
The complete simulation (gait, odometry and camera) is launched by:
```sh
roslaunch hexapod full_simulator.launch
```
the only thing not spawned here is a teleoperation node, keep reading the other paragraph to know how to do it.

Note: if you can't see the shape of the robot on RViz (a common problem in Ubuntu 18 / R.O.S. Melodic), simply add this to the bottom of `~/.bashrc`:
```sh
export LC_NUMERIC="en_US.UTF-8"
```

## Real robot
To start the firmware on the real robot:
```sh
sudo bash
roslaunch hexapod real_robot_base.launch
```
Note that `sudo bash` gives you root privileges, that are mandatory for GPIOs and Serial interfaces.

ALTERNATIVELLY: simply execute `start_firmware.sh`, located in the root directory of this repository.

### RealSense Camera (RS200 - only on Ubuntu 16.04 or below)
First start the camera handler with `roslaunch hexapod r200_handler.launch`, then its depth processing with `roslaunch hexapod depth_processing.launch`, then you can look at the produced pointcloud in Rviz by typing `rviz -d rviz/realsense_rgbd_pointcloud.rviz` or use RtabMap with `roslaunch hexapod rtabmap.launch`.

### Razor IMU
[Razor IMU M0](https://learn.sparkfun.com/tutorials/9dof-razor-imu-m0-hookup-guide) is used to obtain linear accelerations and attitude of the real robot.
To use it in ROS a custom version is needed; to install it on a new IMU board, first install on your host pc the correct Arduino Board (following the guide above), then flash the custom firmware as explained [here](https://www.jetsonhacks.com/2016/07/01/jetson-racecar-part-9-razor-imu-ros-install/) or [here](http://wiki.ros.org/razor_imu_9dof) using Arduino IDE.

Now clone `https://github.com/Maik93/razor_imu_9dof` in workspace's source. Build the workspace with catkin. If you want to test it right now (not mandatory), cd on it, make a copy of `config/my_razor.yaml` by typing `cp ~/aragog_ws/src/hexapod/config/my_razor.yaml ~/aragog_ws/src/razor_imu_9dof/config/my_razor.yaml` and change its first line with the correct port (usually `ttyACM*` in Ubuntu).

You can then publish IMU data in the topic `/imu` by running `roslaunch razor_imu_9dof razor-pub.launch` or in the topic `/imu_data` by running `roslaunch hexapod imu.launch` (if you get permission errors, make sure your user is part of `dialout` group).

If you'd like to have a visualization of the IMU (optional), onestly I don't know... `razor-display.launch` is based on a deprecated library, I've tried [this fix](https://github.com/AhmedAbdlhamid/IMU_Razor9DOF) as suggested [here](https://github.com/KristofRobot/razor_imu_9dof/issues/42#issuecomment-513631021) but I've found conflicts between Python 2 and 3 so I dropped the idea.

## Control it
When Gazebo or the real robot are running (spowned by the above commands), you can directly control hexapod's body pose publishing in the corresponding topic:
```sh
rosrun hexapod inverse_kinematic.py 0 0 0.1 0 0 0
```

### Control via keyboard
```sh
rosrun hexapod teleop_key.py
```

### Control via PS3 joystick
First of all, install the needed drivers: http://wiki.ros.org/ps3joy
You have to be superuser when execute `teleop_ps3` node, so write:
```sh
sudo bash
rosrun hexapod teleop_ps3.py
```

### Control via ArbotiX Commander
The [ArbotiX Commander](https://learn.trossenrobotics.com/index.php/getting-started-with-the-arbotix/12-arbotix-commander-setup) is an Arduino-based joystick that communicate through XBee.
The [official firmware](https://github.com/Interbotix/arbotix/blob/master/ArbotiX%20Sketches/Commander/Commander.ino) is supported, to interface with it just type:
```sh
sudo bash
rosrun hexapod teleop_commander.py
```

PS: `roslaunch` and `rosrun` can run from any dir.

## Start at boot (on Ubuntu)
`Systemd` is used to launch `real_robot_base.launch` at system startup (through `start_firmware.sh` script). To make it able to do so, just link the service file in systemd dir and enable it:
```sh
sudo cp hexapod_firmware.service /etc/systemd/system/
sudo systemctl enable hexapod_firmware.service
```
To test it without reboot:
```sh
sudo systemctl start hexapod_firmware.service
sudo systemctl status hexapod_firmware.service # "journalctl -xe" for more info
```
